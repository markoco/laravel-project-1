<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use\Session;
class ItemController extends Controller
{
   public function index(){
	   	$items = Item::all();
        $categories = Category::all();
	   	return view('catalog', compact('items','categories'));
   }

    public function additem(){
	    $categories = Category::all();
	   	return view('adminviews.additem', compact('categories'));
   }

     public function store(Request $req){
    	//Validate

    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"imgPath" => "required|image|mimes:jpeg,png,jpg,gif,tiff,tif,webp"
     	);

     	$this->validate($req,$rules);

     	//Capture
     	// dd($req);	
     	$newItem = new Item;
     	$newItem->name = $req->name;
     	$newItem->description = $req->description;
     	$newItem->price = $req->price;
     	$newItem->category_id = $req->category_id;

     	//Image Handling
     	$image = $req->file('imgPath');

     	//We'll rename the image
     	$image_name = time().".".$image->getClientOriginalExtension();

     	// Correspond with Public Image Directory
     	$destination = "images/";
     	$image->move($destination,$image_name);
     	$newItem->imgPath = $destination.$image_name;

     	//Save
     	$newItem->save();
     	Session::flash("message" , "$newItem->name has been added");

     	//Redirect
     	return Redirect ('catalog');

    }

      public function destroy($id){
    	$itemToDelete = Item::find($id);
    	$itemToDelete->delete();
    	return redirect('/catalog');
    }

      public function update($id){
    	$item = Item::find($id);
    	$categories = Category::all();
    	return view('adminviews.edititem', compact('item','categories'));
    }

    public function edit($id, Request $req){
    	$item = Item::find($id);

    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"imgPath" => "image|mimes:jpeg,png,jpg,gif,tiff,tif,webp"
    	);

    	$this->validate($req, $rules);
    	$item->name = $req->name;
     	$item->description = $req->description;
     	$item->price = $req->price;
     	$item->category_id = $req->category_id;

     	if($req->file('imgPath') != null){
	     	//Image Handling
	     	$image = $req->file('imgPath');

	     	//We'll rename the image
	     	$image_name = time().".".$image->getClientOriginalExtension();

	     	// Correspond with Public Image Directory
	     	$destination = "images/";
	     	$image->move($destination,$image_name);
	     	$item->imgPath = $destination.$image_name;
     	}

     	$item->save();
     	Session::flash('message', "$item->name has been updated");
     	return redirect('/catalog');

    }

    public function addToCart($id, Request $req){
    	$item = Item::find($id);
    	//check if there is an existing session
    	if(Session::has('cart')){
    		$cart = Session::get('cart');
    	}else{
    		$cart = [];
    	}
    	//check if this is the first time we'll add the item to cart
   		if(isset($cart[$id])){
   			$cart[$id] += $req->quantity;
   		}else{
   			$cart[$id] = $req->quantity;
   		}

    

   		Session::put("cart", $cart);
   		Session::flash('message', "$req->quantity of $item->name has been added");
   		return redirect()->back();
    }

    public function showCart(){
        //we will create a new array containing item name, price, quantity and subtotal
        $items = [];
        $total = 0;

        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach ($cart as $itemId => $quantity) {
                $item = Item::find($itemId);
                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;
                $items[] = $item; //This is how to push in PHP
                $total += $item->subtotal;
            }
        }
        return view('userviews.cart', compact('items','total'));
    }

    public function removeitem($id){
        // unset($_SESSION[id]); this is how to unset session in php
        // $cart = Session::get("cart.$id"); //This is how to call a session, make sure na nasa double quote para mabasa yung $id na variable
        Session::forget("cart.$id");
        return redirect()->back();
    }

    public function emptycart(){
        Session::forget("cart");
        return redirect()->back();
    }

    public function filter($id){
        $items = Item::where('category_id', $id)->get();
        $categories = Category::all();
        return view('catalog', compact('items','categories'));
    }

    public function sort($sort){
        $items = Item::orderBy('price', $sort)->get();
        $categories = Category::all();
        return view('catalog', compact('items','categories'));
    }

    public function search(Request $req){
        $items= Item::where('name', 'LIKE', '%' . $req->search .'%')->orWhere('description','LIKE', '%' . $req->search . '%')->get();
        $categories = category::all();

        if(count($items)==0){
            Session::flash('message', 'No Item Found');
        }

        return view('catalog', compact('items', 'categories'));
    }
}