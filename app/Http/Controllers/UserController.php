<?php

namespace App\Http\Controllers;
use\App\User;
use\App\Order;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
    	$users = User::all();
    	$orders = Order::all(); 
    	$usersWithOrders = $orders->pluck('user_id')->toArray();

    	// dd($usersWithOrders);
    	return view('adminviews.users',compact('users','usersWithOrders'));
    }

     public function changeRole($id){
    	$user = User::find($id);

    	if($user->role_id ==2){
    		$user->role_id = 1;
    	}else{
    		$user->role_id = 2;
    	}
    	$user->save();
    	return redirect()->back();
    }

    public function deleteUser($id){
    	$userToDelete = User::find($id);
    	$userToDelete->delete();
    	return redirect()->back();
    }
    //  public function destroy($id){
    // 	$itemToDelete = Item::find($id);
    // 	$itemToDelete->delete();
    // 	return redirect('/catalog');
    // }
}
