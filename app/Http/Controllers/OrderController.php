<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Order;
use \App\Item;
use \App\User;
use Session;
class OrderController extends Controller
{
    public function checkout(){
    	//Save Transaction in the order table
    	//Make sure that there is a user logged in
    	//Populate our item_order
    	if(Auth::user()){
    		$order = new Order;
    		$order->user_id = Auth::user()->id;
    		$order->payment_id = 1;
    		$order->status_id = 1;
    		$order->total = 0;
    		$order->save();

    		$cart = Session::get('cart');
    		$total = 0;
    		foreach ($cart as $itemId => $quantity) {
    			$order->items()->attach($itemId, ["quantity"=>$quantity]); //items() is the function created in model. Attach is used to automatically get the id of $order. i method used to insert data in item_order. ["pivotColumnName", $value]
    			$item = Item::find($itemId);
    			$total += $item->price * $quantity;

    		}

    		$order->total = $total;
    		$order->save();

    		Session::forget('cart');
    		Session::flash('message', "Order Successfully Placed");
    		return redirect('/catalog');
    			//Syntax for adding entries to our many to many table
    	}else{
    		return redirect('/login');
    	}
    }

    public function showOrders(){
    	$orders = Order::where('user_id', Auth::user()->id)->get(); //dapat laging may get pag gumamit ng where

    	return view('userviews.orders', compact('orders'));
    }

    public function allOrders(){
        $orders = Order::all(); //dapat laging may 
        return view('adminviews.allorders', compact('orders'));
    }

    public function cancelOrder($id){
        $order = Order::find($id);
        $order->status_id=4;
        $order->save();

        return redirect()->back();
    }

}
