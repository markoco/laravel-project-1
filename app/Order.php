<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function status(){
    	return $this->belongsTo("App\Status");
    }

    public function payment(){
    	return $this->belongsTo("App\Payment"); //You can also use Use instead of ("App\Payent")
    }

    public function items(){
    	return $this->belongsToMany("\App\Item")->withPivot("quantity")->withTimeStamps();
    }

     public function user(){
    	return $this->belongsTo("App\User"); //You can also use Use instead of ("App\Payent")
    }
}
