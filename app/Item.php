<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
   public function category(){
   	return $this->belongsTo("\App\Category");
   }

   public function orders(){
   	return $this->belongsToMany("\App\Order")->withPivot("quantity")->withTimeStamps();//For many to many relationships use withPivot if you added new column to the intermediary table
   }
}