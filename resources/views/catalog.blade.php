
@extends("layouts.app")
@section('content')

	@if(Session::has("message"))
		<h4>{{Session::get('message')}}</h4>
	@endif

	<div class="container-fluid">
		<h1 class="text-center py-5">Online Shopping</h1>
		<div class="d-flex justify-content-end">
			<div class="col-lg-5">
				<form class="p-3" action="/search"  method="POST">
					@csrf
					<div class="input-group">
						<input type="text" name="search" class="form-control" placeholder="Search items by name, category">
						<div class="input-group-append">
							<button class="btn btn-info">Search</button>
						</div>
					</div>	
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
				<h4>Filter By Category</h4>
				<ul class="list-group">
					@foreach($categories as $category)
						<li class="list-group-item">
							<a href="/catalog/{{$category->id}}">{{$category->name}}</a>
						</li>
					@endforeach
						<li class="list-group-item">
							<a href="/catalog">All</a>
						</li>
				</ul>
				<hr>
				<h4>Sort by Price</h4>
				<ul class="list-group">
					<li class="list-group-item"><a href="/catalog/sort/asc">Cheapest First</a></li>
					<li class="list-group-item"><a href="/catalog/sort/desc">Most Expensive First</a></li>
				</ul>
			</div>
			<div class="col-lg-10">
				<div class="row w-100">
				@foreach($items as $indiv_item)
					<div class="col-lg-4 p3 my-2">
					<div class="card">
						<img src="{{asset($indiv_item->imgPath)}}" alt="" class="card-img-top" height="300px">
						<div class="card-body">
							<h2 class="card-title">{{$indiv_item->name}}</h2>
							<p class="card-text">{{$indiv_item->description}}</p>
							<p class="card-text">Price: {{$indiv_item->price}}</p>
							<p class="card-text">Category: {{$indiv_item->category->name}}</p>
						</div>
						<div class="card-footer">
							@Auth
							@if(Auth::user()->role_id == 1)
							<div class="d-flex my-2">
								<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
									@csrf
									@method('DELETE')
									<button class="btn btn-danger">Delete Item</button>
								</form>
								<a href="/updateitem/{{$indiv_item->id}}" class="btn btn-success mx-2">Edit Item</a>
							</div>
							@else
							<div class="card-footer">
								{{-- <form action="/addtocart/{{$indiv_item->id}}" method="POST">
									@csrf --}}
									<input type="number" name="quantity" class="form-control" value="1" id="quantity_{{$indiv_item->id}}">
									<button class="btn btn-primary" onclick="addToCart({{$indiv_item->id}})" type="submit">Add to cart</button>
								{{-- </form> --}}
							</div>
							@endif
							@endAuth	
						</div>
					</div>
					</div>
				@endforeach
			</div>
			</div>
		</div>
	</div>

	

	<script>
		const addToCart = id=>{
			let quantity = document.querySelector("#quantity_"+id).value;
			alert(quantity+ "of item" + id + "has been added to cart");
			let data = new FormData;
			data.append("_token", "{{csrf_token()}}");
			data.append("quantity", quantity)

			fetch("/addtocart/"+id,{
				method: "POST",
				body: data
			})
		}



	</script>
@endsection
