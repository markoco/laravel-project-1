@extends('layouts.app')
@section('content')
	<table class="table table-striped mx-auto">
		<thead>
			<tr>
				<th>Order Id</th>
				<th>Order Date</th>
				<th>Order Details</th>
				<th>Order Total</th>
				<th>Customer Name</th>
				<th>Status</th>
				<th>Payment</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td>{{$order->id}}</td>
				<td>{{$order->created_at}}</td>
				<td>
					{{-- @dd($order->items()) --}}
					@foreach($order->items as $item)
					Name:{{$item->name}}, Qty: {{$item->pivot->quantity}}
					<br>
					@endforeach
				</td>
				<td>{{$order->total}}</td>
				<td>{{$order->user->name}}</td>
				<td>{{$order->status->name}}</td>
				<td>{{$order->payment->name}}</td>
				<td>
					<a href="/cancelorder/{{$order->id}}" class="btn btn-danger {{$order->status_id ==4? "disabled" : ""}}">Cancel</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection