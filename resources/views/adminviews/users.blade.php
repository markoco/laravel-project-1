@extends('layouts.app')

@section('content')
	<h1 class="text-center py-5">All Users</h1>
	<div class="col-lg-10 offset-lg-1">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Name:</th>
					<th>Email Address:</th>
					<th>Role:</th>
					<th>Action:</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td>{{$user->name}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->role->name}}</td>
						<td>
							@if($user->role_id == 1)
								<a href="/changerole/{{$user->id}}" class="btn btn-danger">Demote</a>
							@else
								<a href="/changerole/{{$user->id}}" class="btn btn-info">Promote</a>
							@endif
							{{-- in array checks if user  exists in $usersWith orders--}}
							@if(in_array($user->id, $usersWithOrders) || $user->role_id == 1)
								
							@else
								<form action="/deleteuser/{{$user->id}}" method="POST">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger">Delete</a>
								</form>	
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection