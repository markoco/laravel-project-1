@extends('layouts.app')
@section('content')
<div class="container">
	<h1 class="text-center py-5">Edit Item Page</h1>
	<div class="row">
		<div class="col-lg-6 offset-lg-3 bg-dark">
			<form action="/edititem/{{$item->id}}" method="POST" class="text-white p-5" enctype="multipart/form-data">
				@csrf
				@method('PATCH')
				<div class="form-group">
					<label for="name">Item Name</label>
					<input type="text" name="name" class="form-control" value="{{$item->name}}">
				</div>
				<div class="form-group">
					<label for="description">Item Description</label>
					<input type="text" name="description" class="form-control" value="{{$item->description}}">
				</div>
				<div class="form-group">
					<label for="price">Item Price</label>
					<input type="number" name="price" class="form-control" value="{{$item->price}}">
				</div>
				<div class="form-group">
					<label for="imgPath">Item Image</label>
					<input type="file" name="imgPath" class="form-control">
				</div>
				<div class="form-group">
					<label for="category_id">Item Category</label>
					<select name="category_id" class="form-control">
						@foreach($categories as $indiv_category)
						<option value="{{$indiv_category->id}}"
							{{$item->category_id == $indiv_category->id? "selected" : ""}}>{{$indiv_category->name}}</option>
						@endforeach
					</select>
				</div>
				<button class="btn btn-success" type="submit">Edit Item</button>
		</form>
		</div>
	</div>
</div>
@endsection