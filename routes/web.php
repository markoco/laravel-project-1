<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ItemController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/catalog', 'ItemController@index');
Route::get('/catalog/{id}', 'ItemController@filter');
Route::get('/catalog/sort/{sort}', 'ItemController@sort');
Route::post('/search', 'ItemController@search');

Route::middleware("admin")->group(function(){
	Route::get('/additem', 'ItemController@additem');
	Route::post('/additem', 'ItemController@store');
	Route::delete('/deleteitem/{id}', 'ItemController@destroy');
	Route::get('/updateitem/{id}', 'ItemController@update');
	Route::patch('/edititem/{id}', 'ItemController@edit');
	Route::get('/allorders', 'OrderController@allOrders');
	// Route::get('/cancelorder/{id}', 'OrderController@cancelOrderByAdmin');
	Route::get('/allusers', 'UserController@index');
	Route::delete('/deleteuser/{id}', 'UserController@deleteUser');
	Route::get('/changerole/{id}', 'UserController@changeRole');
});

Route::middleware("user")->group(function(){
		//Cart Crud
	Route::post('/addtocart/{id}', 'ItemController@addToCart');
	Route::get('/showcart', 'ItemController@showCart');
	Route::get('/home', 'HomeController@index')->name('home');
	Route::delete('/removeitem/{id}', 'ItemController@removeitem');
	Route::delete('/emptycart', 'ItemController@emptycart');
	Route::get('/checkout', 'OrderController@checkout');
	// orders
	Route::get('/showorders', 'OrderController@showOrders');
	

});

Route::middleware('auth')->group(function(){
	Route::get('/cancelorder/{id}', 'OrderController@cancelOrder');
});


Route::get('/projects', 'ProjectController@addProjectToUser');



Route::post('/projects', 'ProjectController@saveProject');







